// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var diagrama = $(".chart"),
	counter  = $(".counter"),
	video 	 = $('.video__wrapper');

if(diagrama.length){
  include("js/diagram.js");
}

if(counter.length){
  include("js/counter.js");
}

if(video.length){
  include("js/video.js");
}

// if($(".data-mh").length){
//   include("js/jquery.matchHeight-min.js");
// }
// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){



      if(diagrama.length){

        diagrama.percentcircle({
          	animate : true,
      			diameter : 100,
      			guage: 3,
      			coverBg: '#fff',
      			bgColor: '#cccccc',
      			percentSize: '18px',
      			percentWeight: 'normal'
        });

      }

});

$(window).load(function(){
	
	if(counter.length){

        counter.countTo({
            from: 3,
            to: 1,
            speed: 2500,
            refreshInterval: 50,
            onComplete: function(value) {
                console.debug(this);
            }
        });
	}

	setTimeout(function() {
		$('body').find('.counter').hide();
		$('body').find('.video__wrapper').addClass("active");
	}, 3000);
});